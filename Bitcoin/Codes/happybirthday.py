from sys import exit
from bitcoin.core.script import *
from lib.utils import *
import hashlib as hash
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from ex1 import send_from_P2PKH_transaction

######################################################################
message = 'Happy Birthday Hamed'.encode()
Q2a_txout_scriptPubKey = [
    OP_RETURN, message
    ]
######################################################################

if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.00082911 - 0.00005 # amount of BTC in the output you're splitting minus fee
    txid_to_spend = (
        '5df2bd0951b1d436c8fb77045d29e1cf419b6545dcb8508ac28140ee65436dd3')
    utxo_index = 14# index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(
        amount_to_send, txid_to_spend, utxo_index,
        Q2a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)