from sys import exit
from bitcoin.core.script import *
from lib.utils import *
import hashlib as hash
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from ex1 import send_from_P2PKH_transaction
from bitcoin.wallet import CBitcoinSecret, P2PKHBitcoinAddress

######################################################################
hamed_secretkey = CBitcoinSecret.from_secret_bytes(hash.sha256('happy birthday Hamed , your uncle saeed'.encode()).digest())
hamed_publickey = hamed_secretkey.pub
hamed_address = P2PKHBitcoinAddress.from_pubkey(hamed_publickey)
Q4a_txout_scriptPubKey = [
    1577484000, OP_CHECKLOCKTIMEVERIFY, OP_DROP, OP_DUP, OP_HASH160, hamed_address, OP_EQUALVERIFY,
    OP_CHECKSIG
    ]
######################################################################

if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.00082911 - 0.00005 # amount of BTC in the output you're splitting minus fee
    txid_to_spend = (
        '5df2bd0951b1d436c8fb77045d29e1cf419b6545dcb8508ac28140ee65436dd3')
    utxo_index = 13# index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(
        amount_to_send, txid_to_spend, utxo_index,
        Q2a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)