from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (faraz_private_key, ata_private_key, shareholder1_private_key,
                    shareholder2_private_key, shareholder3_private_key,
                    shareholder4_private_key, shareholder5_private_key,
                    network_type, faucet_address)
from ex1 import P2PKH_scriptPubKey
from ex2a import Q2a_txout_scriptPubKey
from ex31a import Q3a_txout_scriptPubKey



def multisig_scriptSig(txin, txout, txin_scriptPubKey):
    faraz_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             faraz_private_key)
    ata_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             ata_private_key)
    shareholder1_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             shareholder1_private_key)
    shareholder2_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             shareholder2_private_key)
    shareholder3_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             shareholder3_private_key)
    shareholder4_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             shareholder4_private_key)
    shareholder5_signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             shareholder5_private_key)

    ######################################################################
    # TODO: Complete this script to unlock the BTC that was sent to you
    # in the PayToPublicKeyHash transaction.
    return [
        OP_0, ata_signature, OP_0, shareholder2_signature, shareholder3_signature, shareholder4_signature
    ]
def send_from_multisig_trans(
        amount_to_send, txid_to_spend, utxo_index,
        txin_scriptPubKey, txout_scriptPubKey, network):

    txout = create_txout(amount_to_send, txout_scriptPubKey)
    txin = create_txin(txid_to_spend, utxo_index)
    txin_scriptSig = multisig_scriptSig(txin, txout, txin_scriptPubKey)
    new_tx = create_signed_transaction(txin, txout, txin_scriptPubKey,
                                       txin_scriptSig)
    return broadcast_transaction(new_tx, network)
######################################################################
# TODO: set these parameters correctly
amount_to_send = 0.00077911 - 0.0001 # amount of BTC in the output you're splitting minus fee
txid_to_spend = (
        '130b8e304e47675d99633795eadc55777b3561830de3a5bb94a043ac308966fa')
utxo_index = 0 # index of the output you are spending, indices start at 0
######################################################################

######################################################################
# TODO: implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.
######################################################################
txin_scriptPubKey = Q3a_txout_scriptPubKey
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

response = send_from_multisig_trans(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, txout_scriptPubKey, network_type)
print(response.status_code, response.reason)
print(response.text)
