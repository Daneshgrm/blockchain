from bitcoin import SelectParams
from bitcoin.base58 import decode
from bitcoin.core import x
from bitcoin.wallet import CBitcoinAddress, CBitcoinSecret, P2PKHBitcoinAddress


SelectParams('testnet')

faucet_address = CBitcoinAddress('mv4rnyY3Su5gjcDNzbMLKBQkBicCtHUtFB')

# For questions 1-3, we are using 'btc-test3' network. For question 4, you will
# set this to be either 'btc-test3' or 'bcy-test'
network_type = 'btc-test3'


######################################################################
# This section is for Questions 1-3
# TODO: Fill this in with your private key.
#
# Create a private key and address pair in Base58 with keygen.py
# Send coins at https://coinfaucet.eu/en/btc-testnet/

my_private_key = CBitcoinSecret(
    'cR4BfyNr2KSsc5bLFdSqdnJcpsMvRFT1EuqEuZ97WEswzmiotSST')
# my_address : mpyGf92LPLn8SAqP9xkVdruTL3QuSfzXzy
# tx: 7e2dfd1fb6fc6703aaca89da2679625e1e81e597d5c961b939140c459b592f78
# return address: mv4rnyY3Su5gjcDNzbMLKBQkBicCtHUtFB
my_public_key = my_private_key.pub
my_address = P2PKHBitcoinAddress.from_pubkey(my_public_key)
######################################################################

# Question 3
faraz_private_key = CBitcoinSecret('cSjpTkwKBd3zsf61d6NP1Ded9Lq9ne9ySGxV1YrfGpcVkvBaWPrA')
ata_private_key = CBitcoinSecret('cTNoawVAbEusajEetbmEK6tELnimVEhStzfXZv1E5vebLZvaM9RV')
shareholder1_private_key = CBitcoinSecret('cPEEQxwbTcj2cGX5RLuZA3PbQL3PopUvkNKcudrc2bitWo15xfKB')
shareholder2_private_key = CBitcoinSecret('cRyzJZgCzFskTjT6xjpANX62nuu7gFgRrpVXAD5fv56j6zRX1evK')
shareholder3_private_key = CBitcoinSecret('cNBKZkKPQ8CRwAb85Wu7KrUyADkx4wRsWHsjUsZj9YzrotKi8pfU')
shareholder4_private_key = CBitcoinSecret('cSQcMoaBX3Q3kuZQXVZL7dmRxAJKsHdwvG4KTHaEEHt88joFrcpW')
shareholder5_private_key = CBitcoinSecret('cQvttpvWeBUb8EFXemFTBDCu2DNGynrXRpDKb91xd9us5ng6R1SL')

######################################################################
# NOTE: This section is for Question 4
# TODO: Fill this in with address secret key for BTC testnet3
#
# Create address in Base58 with keygen.py
# Send coins at https://coinfaucet.eu/en/btc-testnet/

# Only to be imported by alice.py
# Alice should have coins!!
alice_secret_key_BTC = CBitcoinSecret(
    'cRUmTUm2HxQCsEFg12FDRrA498tkpPEbpBbX8C2bfyn7LinQzgds')
# alice_address : mi7aGkTjgDS9JMcvbrXaLoSNYQmHWKXkMx
# tx: 8a7725b8546f430be85adb8aeffe9acedb4bd15bc4d990c8b824f8e847d3bd0f
# return: //
# Only to be imported by bob.py
bob_secret_key_BTC = CBitcoinSecret(
    'cTYzJubkUnbxPTJ6csC4QK2jqHPJr4DWhYtSbcPznSP2bcDBuXRG')
# bob_address : n4JwFQfhJELB4k1UgcKKeE8Wt6kVFP7N9k
# Can be imported by alice.py or bob.py
alice_public_key_BTC = alice_secret_key_BTC.pub
alice_address_BTC = P2PKHBitcoinAddress.from_pubkey(alice_public_key_BTC)

bob_public_key_BTC = bob_secret_key_BTC.pub
bob_address_BTC = P2PKHBitcoinAddress.from_pubkey(bob_public_key_BTC)
######################################################################


######################################################################
# NOTE: This section is for Question 4
# TODO: Fill this in with address secret key for BCY testnet
#
# Create address in hex with
# curl -X POST https://api.blockcypher.com/v1/bcy/test/addrs?token=$YOURTOKEN
# This request will return a private key, public key and address. Make sure to save these.
#
# "private": "c280c530c71d779cb20cd5404d6dea79debe978910ec09824c61bb4d4aed682c",
# "public": "03c973a931820ecab15f08b44fe065a79f3674e7b3d825370c613db89fcd349ac0",
# "address": "CGKHYGrmopqB5X5Q958QbDzPEKi6XYnFFh",
# "wif": "Bur7sGbNMttdjmUckJoyxzskA5i7eEeNqAZ5fZ77z28XEJGCNMuY"
#
# Send coins with
# curl -d '{"address": "BCY_ADDRESS", "amount": 1000000}' https://api.blockcypher.com/v1/bcy/test/faucet?token=<YOURTOKEN>
# This request will return a transaction reference. Make sure to save this.

# Only to be imported by alice.py
alice_secret_key_BCY = CBitcoinSecret.from_secret_bytes(
    x('559a593348f62f53a78bb8b83235dd8a809b96520ab4367f3038289ffa5260a1'))
#
# "private": "559a593348f62f53a78bb8b83235dd8a809b96520ab4367f3038289ffa5260a1",
# "public": "037386e20a6283498fbdff16a7c53160e70514053d1e2e1f9d0c7484553a458ec0",
# "address": "BtE1XVp67qhZUpvJWRGkWrp4nYy7nDZAcg",
# "wif": "BrCRzQBz6zq3ARa2X5RigZiLtZgYsLNAUV1JD8CvQHVAuiDHLctW"
#
# Only to be imported by bob.py
# Bob should have coins!!
bob_secret_key_BCY = CBitcoinSecret.from_secret_bytes(
    x('ff3c6f5c9c2ccd7cf3418fd436cafef8c1a56b5af418758df6c545fcb76c8878'))
#
# "private": "ff3c6f5c9c2ccd7cf3418fd436cafef8c1a56b5af418758df6c545fcb76c8878",
# "public": "0382e104b65b247a9ed7b8382a50601ec5bd895f88accacabc47dbfea922509aea",
# "address": "CFbssBjazZzHKif5t3wxP2hVANjs6PHQUb",
# "wif": "BwtBBVqfjYwZY4kimNQU7fB1Xzp18dfafBBzunV3nQjzSP7umS5T"
#
# "[fund] tx_ref": "e5cacab67c29d5fa444aa45ce5002432f771c529cb9ef403acb9a25ea0dd1b5d"
#
# Can be imported by alice.py or bob.py
alice_public_key_BCY = alice_secret_key_BCY.pub
alice_address_BCY = P2PKHBitcoinAddress.from_pubkey(alice_public_key_BCY)

bob_public_key_BCY = bob_secret_key_BCY.pub
bob_address_BCY = P2PKHBitcoinAddress.from_pubkey(bob_public_key_BCY)
######################################################################
