from sys import exit
from bitcoin.core.script import *
from lib.utils import *
import hashlib as hash
from lib.config import (faraz_private_key, ata_private_key, shareholder1_private_key,
                    shareholder2_private_key, shareholder3_private_key,
                    shareholder4_private_key, shareholder5_private_key,
                    my_private_key, network_type)
from ex1 import send_from_P2PKH_transaction


######################################################################
# TODO: Complete the scriptPubKey implementation for Exercise 2

Q3a_txout_scriptPubKey = [
     OP_DEPTH, 2, OP_SUB, OP_IF, 3, shareholder1_private_key.pub, shareholder2_private_key.pub, shareholder3_private_key.pub,
        shareholder4_private_key.pub, shareholder5_private_key.pub, 5, OP_CHECKMULTISIGVERIFY, 1, OP_ELSE, 2, OP_ENDIF, faraz_private_key.pub,
        ata_private_key.pub, 2, OP_CHECKMULTISIG
]
######################################################################

if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.00082911 - 0.00005 # amount of BTC in the output you're splitting minus fee
    txid_to_spend = (
        '5df2bd0951b1d436c8fb77045d29e1cf419b6545dcb8508ac28140ee65436dd3')
    utxo_index = 9 # index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(
        amount_to_send, txid_to_spend, utxo_index,
        Q3a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    # print(response.text)
    print()
