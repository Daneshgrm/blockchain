from bitcoin.core.script import *

######################################################################
# These functions will be used by Alice and Bob to send their respective
# coins to a utxo that is redeemable either of two cases:
# 1) Recipient provides x such that hash(x) = hash of secret
#    and recipient signs the transaction.
# 2) Sender and recipient both sign transaction
#
# TODO: Fill these in to create scripts that are redeemable by both
#       of the above conditions.
# See this page for opcode documentation: https://en.bitcoin.it/wiki/Script

# This is the ScriptPubKey for the swap transaction
def coinExchangeScript(public_key_sender, public_key_recipient, hash_of_secret):
    return [
        OP_IF, OP_HASH160, hash_of_secret, OP_EQUALVERIFY, public_key_recipient, OP_CHECKSIG, OP_ELSE, 1577468700, OP_CHECKLOCKTIMEVERIFY,
        OP_DROP, 2, public_key_sender, public_key_recipient, 2, OP_CHECKMULTISIG, OP_ENDIF
        # fill this in!
    ]

# This is the ScriptSig that the receiver will use to redeem coins
def coinExchangeScriptSig1(sig_recipient, secret):
    return [
        sig_recipient, secret, OP_1
    ]

# This is the ScriptSig for sending coins back to the sender if unredeemed
def coinExchangeScriptSig2(sig_sender, sig_recipient):
    return [

        OP_0, sig_sender, sig_recipient, OP_0
    ]
######################################################################

######################################################################
#
# Configured for your addresses
#
# TODO: Fill in all of these fields
#

alice_txid_to_spend     = "831bae7f2a0855f967a3f7c57896a5d6c8258f10090f65dc71c92868ea937584"
alice_utxo_index        = 0
alice_amount_to_send    = 0.00107167

bob_txid_to_spend       = "e3db6d4517536bbc045f5ccfbb6be4e197c7b4192331f61b1be48deb273d1e3f"
bob_utxo_index          = 0
bob_amount_to_send      = 0.00031666

# Get current block height (for locktime) in 'height' parameter for each blockchain (and put it into swap.py):
#  curl https://api.blockcypher.com/v1/btc/test3
btc_test3_chain_height  = 1635233

#  curl https://api.blockcypher.com/v1/bcy/test
bcy_test_chain_height   = 2673288

# Parameter for how long Alice/Bob should have to wait before they can take back their coins
## alice_locktime MUST be > bob_locktime
alice_locktime = 5
bob_locktime = 3

tx_fee = 0.0001

# While testing your code, you can edit these variables to see if your
# transaction can be broadcasted succesfully.
broadcast_transactions = False
alice_redeems = True

######################################################################
